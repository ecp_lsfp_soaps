#!/usr/bin/env ruby

require 'rubygems'
require "bundler/setup"
require 'active_support/all'
require 'sqlite3'

require 'yaml'
require 'simplews'
require 'pp'
require 'uri'
require 'json'

require 'nokogiri'

class LsfProxySoapServer < SimpleWS

  class NonExistentCourseError < ::SOAP::Error; end 
  class NoCourseUrlError < ::SOAP::Error; end 

  DB_FILE = 'notifications.db'
  DB_TABLE_NAME = 'notifications'

  def initialize
    doc = YAML::load_stream(File.open(File.expand_path("../config.yml", __FILE__)))
    @config_soap_server = doc[0] 
    url= URI::parse(@config_soap_server['soap']['url'])
    super(@config_soap_server['soap']['name'],
          @config_soap_server['soap']['info'],
          url.host, url.port)
 
    # initializing logger
    logfilepath= @config_soap_server['logger']['filename']
    directory_check(File.dirname(logfilepath),"Logfile", false)
    if not absolute_path?(logfilepath)
      logfilepath= File.expand_path("../"+logfilepath, __FILE__)
    end
    logfile = File.open(logfilepath, File::WRONLY|File::APPEND|File::CREAT, 0600)
    logfile.sync= true
    @log = Logger.new(logfile,5,100.kilobytes)
    @log.formatter = proc do |severity, datetime, progname, msg|
      "[%s%6s] %s\n" % [datetime.to_s(:db), severity, msg.dump]
    end
    @log.level = eval @config_soap_server['logger']['level']
    
    # notifications database
    if File.exist? DB_FILE
      @db = SQLite3::Database.open DB_FILE
    else
      @db = SQLite3::Database.new DB_FILE
    end
    @db.execute "CREATE TABLE IF NOT EXISTS #{DB_TABLE_NAME}(Id INTEGER PRIMARY KEY, 
                obj_name TEXT, obj_id TEXT, proto_id TEXT)"

    trap('INT'){
      @log.info("Stopping server..")
      self.shutdown
    }
  
    self.serve :lsfPushData, %w(objectName objectId protocolId), 
          :objectName => :string,
          :objectId => :string,
          :protocolId => :string,
          :return => :string
    self.serve :listNotifications
    self.serve :fetchNotification
    self.serve :deleteNotification
    self.serve :getTime
    self.serve :ping
    self.serve :getDataXML, %w(xmlParams), :xmlParams => :string, :return => :string
    self.serve :getCourseUrl, %w(lectureId), :lectureId => :string, :return => :string
  
    wsdl_filename_path= @config_soap_server['soap']['wsdl_filename']
    directory_check(File.dirname(wsdl_filename_path),"WSDL-File")
    if not absolute_path?(wsdl_filename_path)
      wsdl_filename_path= File.expand_path("../" + wsdl_filename_path, __FILE__)
    end
    @log.info "WSDL path: " + wsdl_filename_path
    self.wsdl wsdl_filename_path

    File.open("server.pid", "w") do |f|
      f.puts Process.pid.to_s
    end
    @log.info "LSF-SOAP-Server started (PID: #{Process.pid})"
  end

  def lsfPushData(obj_name, obj_id, proto_id)
      puts "#{obj_name}, #{obj_id}. #{proto_id}"
      @db.execute "INSERT INTO #{DB_TABLE_NAME}(obj_name,obj_id,proto_id) VALUES ('#{obj_name}', '#{obj_id}', '#{proto_id}')"
      @log.info "new notification: object_name:#{obj_name}, object_id:#{obj_id}, protocol_id:#{proto_id}"
  end

  # Course URL
  def getCourseUrl(lectureId)
    result= {
            :url => "http://freeit.de/documents/ecsa/index.html",
            :online_status => true
         }
    case lectureId
      when "123456788"
        text= "There is no course available with the lecture-id: #{lectureId}"
        @log.error text
        raise NonExistentCourseError, text, []
      when "123456789"
        text= "There is no course URL available representing lecture-id: #{lectureId}"
        @log.error text
        raise NoCourseUrlError, text, []
      else
        @log.info "#getCourseUrl: lectureId=#{lectureId}  url=#{result[:url]}  online_status=#{result[:online_status]}"
        result.to_json
    end
  end

  # LSF Mockup
  def getDataXML(xml_params)
    xml_doc= Nokogiri::XML(xml_params)
    case xml_doc.css('SOAPDataService > general > object').text
      when @config_soap_server['lsf']['db_interface']['lecture']
        lsf_mock_lecture(xml_doc.css('SOAPDataService > condition > LectureID').text)
      when @config_soap_server['lsf']['db_interface']['enrollments']
        lsf_mock_enrollments(xml_doc.css('SOAPDataService > condition > LectureID').text)
      when @config_soap_server['lsf']['db_interface']['course_catalog']
        lsf_mock_course_catalog(xml_doc.css('SOAPDataService > condition > TermID').text)
      #when @config_soap_server['lsf']['db_interface']['institutions']
    end
  end 


  def listNotifications
    rows= @db.execute("select * from notifications order by id ASC")
    if rows.blank?
      nil
    else
      rows.each do |row|
        puts row.values_at(1,2,3).join(",")
      end
    end
  end

  def fetchNotification
    row= @db.execute("select * from notifications order by id ASC limit 1")[0]
    if row.blank?
      nil
    else
      id= row[0]
      @db.execute("delete from notifications where id = #{id}")
      row.values_at(1,2,3).join(",")
    end
  end

  def deleteNotification
    row= @db.execute("select * from notifications order by id ASC limit 1")[0]
    if row.blank?
      nil
    else
      @db.execute("delete from notifications where id = #{row[0]}")
    end
  end

  def getTime
    Time.now
  end

  def ping
    return true
  end

private

  # Test if pathname represent an absolute path or not.
  def absolute_path?(pathname)
    (pathname.slice(0,1)== "/") ? true : false
  end

  # Raise exeption when directory is not accessable/available
  def directory_check(pathname, context=nil, logging=true)
    if not File.directory?(pathname)
      if context
        err_text= "#{context}: Directory #{pathname} not accessable/available."
      else
        err_text= "Directory #{pathname} not accessable/available."
      end
      @log.fatal(err_text) if logging
      raise Exception, err_text
    end
    true
  end

  def lsf_mock_lecture(lecture_id)
    case lecture_id
      when "132742"
        IO.read('test_lsf_xml_files/lectures_132742.xml')
      when "132743"
        IO.read('test_lsf_xml_files/lectures_132743.xml')
      when "132634"
        IO.read('test_lsf_xml_files/lectures_132634_multiple_dates_and_groups.xml')
      when "132635"
        IO.read('test_lsf_xml_files/lectures_132635_multiple_dates_no_groups.xml')
      when "35246"
        IO.read('test_lsf_xml_files/lectures_35246_hsz.xml')
      when "26703"
        IO.read('test_lsf_xml_files/lectures_26703_hsz.xml')
      when "empty"
        IO.read('test_lsf_xml_files/lectures_empty.xml')
      else
        "not available"
    end
  end

  def lsf_mock_enrollments(lecture_id)
    case lecture_id
      when "132742"
        IO.read('test_lsf_xml_files/enrollments_132742.xml')
      when "132743"
        IO.read('test_lsf_xml_files/enrollments_132743.xml')
      when "132634"
        IO.read('test_lsf_xml_files/enrollments_132634.xml')
      when "132635"
        IO.read('test_lsf_xml_files/enrollments_132634.xml')
      else
        "not available"
    end
  end

  def lsf_mock_course_catalog(term_id)
    case term_id
      when "20131"
        IO.read('test_lsf_xml_files/course_catalog_20131.xml')
      when "20132"
        IO.read('test_lsf_xml_files/course_catalog_20132.xml')
      when "-1"
        IO.read('test_lsf_xml_files/course_catalog_20131.xml')
      when "empty"
        IO.read('test_lsf_xml_files/course_catalog_empty.xml')
      else
        "not available"
    end
  end

end

begin
  LsfProxySoapServer.new.start
rescue SQLite3::Exception => e 
  puts "Error in accessing database file @{NOTIFICATIONS_DB}"
  puts e
rescue Exception => e
  puts e
  puts e.backtrace
ensure
  db.close if $db
end
